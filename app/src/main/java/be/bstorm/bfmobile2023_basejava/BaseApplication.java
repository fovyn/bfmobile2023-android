package be.bstorm.bfmobile2023_basejava;

import android.app.Application;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;

public class BaseApplication extends Application {

    private String user = "Admin";

    public String getUser() { return this.user; }

    @Override
    public void onCreate() {
        super.onCreate();
        System.out.println();
        Log.d("Application", this.user);
    }
}
