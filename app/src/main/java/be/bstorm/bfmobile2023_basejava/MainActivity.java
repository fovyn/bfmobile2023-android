package be.bstorm.bfmobile2023_basejava;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import be.bstorm.bfmobile2023_basejava.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
    }

}